const express = require('express')
const app = express()
const port = 3000

var mysql = require("mysql");
var connection = mysql.createConnection({
    host: "db",
    port: 3306,
    user: "root",
    password: "root",
    database: "challenge"
});

connection.connect((err) => {
    if (err) {
      console.log("Error occurred", err);
    } else {
      console.log("Connected to MySQL Server");
    }
});

app.get('/:name', (req, res) => {
  const name = mysql.escape(req.params.name)
  connection.query(`INSERT INTO people (name) VALUES (${name})`)
  connection.query('SELECT name FROM people', function(errors, results) {
    let names = []
    for (let index in results) {
      names.push(results[index].name)
    }
    res.send(`<h1>FullCycleRocks!</h1> names: ${names.toString()}`)
  })
})

// JUST TO CLEAN DATABASE WITHOUT EFFORT
app.delete('/', (req, res) => {
  connection.query('DELETE FROM people')
  res.send('DONE')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})